package com.example.mobilesector;

import android.os.Bundle;
import java.io.*;
import java.net.URI;
import java.util.logging.Logger;

import android.net.*;
import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugins.GeneratedPluginRegistrant;

import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.v4.content.FileProvider;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "org.lamboz.www/interfaces";

    @Override
  protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);
        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
            new MethodCallHandler() {
                @Override
                public void onMethodCall(MethodCall call, Result result) {
                    if (call.method.equals("getBatteryLevel")) {
                        int batteryLevel = getBatteryLevel();

                        if (batteryLevel != -1) {
                            result.success(batteryLevel);
                        } else {
                            result.error("UNAVAILABLE", "Battery level not available.", null);
                        }
                    } else if (call.method.equals("getFilesDir")) {
                        String myDir = getApplicationContext().getExternalFilesDir(null).getAbsolutePath();
                        result.success(myDir);
                    } else if (call.method.equals("shareSvgSector")) {
                        String sectorName = call.argument("sectorName");
                        String sectorFile = call.argument("sectorFile");
                        String success = shareSvgSector(sectorName, sectorFile);
                        if (success != "OK") 
                        {
                            result.error ("KO",success,null);
                        }
                        else {
                            result.success("OK");
                        }
                    } else {
                        result.notImplemented();
                    }
                }               
            }
        );
    }

    private String shareSvgSector (String sectorName, String theFile) {
        try {
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            File fileWithinMyDir = new File(theFile);
            
            if(fileWithinMyDir.exists()) {
                intentShareFile.setType("application/octet-stream");
                File basicFile = new File(theFile);
                Uri uriFile = FileProvider.getUriForFile(this, "net.lamboz.swnsector", basicFile);
                System.out.println(uriFile.getPath());
                intentShareFile.putExtra(Intent.EXTRA_STREAM, uriFile);
            
                intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                                    "Data for sector "+sectorName);
                intentShareFile.putExtra(Intent.EXTRA_TEXT, "This is the map of sector "+sectorName);
                startActivity(Intent.createChooser(intentShareFile, "Share sector map"));
                return "OK";
            }
            else {
                return "KO";

            }
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    private int getBatteryLevel() {
    int batteryLevel = -1;
    if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
        BatteryManager batteryManager = (BatteryManager) getSystemService(BATTERY_SERVICE);
        batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
    } else {
        Intent intent = new ContextWrapper(getApplicationContext()).
            registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        batteryLevel = (intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100) /
            intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
    }

    return batteryLevel;
    }

}



