/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';
import 'dart:async';
import 'dart:io';


typedef void SelectedFileCallback(String path);

class SWNSectorFileChooser extends StatefulWidget {
  final List<FileSystemEntity> files;
  final SelectedFileCallback onFileTap;
  SWNSectorFileChooser({this.files, this.onFileTap});

  @override createState() => new SWNSectorFileChooserState();
} 

class SWNSectorFileChooserState extends State<SWNSectorFileChooser> {

  List<ListTile> _getTiles() => this.widget.files.map( (fileEntity) {
    var path = fileEntity.path;
    var txt = path.split("/").last;
    return new ListTile(
      title: new Text(txt),
      subtitle: new Text(path),
      onTap: () {
        if (widget.onFileTap != null)
          widget.onFileTap(path);
      },
    );
  }).toList();

  @override
  Widget build (BuildContext context) {
    return new ListView(
      children: _getTiles()
    );
  }
}

Future<String> fileDialog (String message, String extension, BuildContext ctx) async {
  final dir = await getApplicationJsonPathDir();      
  final list = dir.listSync();
  final List<FileSystemEntity> _files = list.where ( (f) => f.path.endsWith(extension) ).toList();
  String res = await Navigator.of(ctx).push(
    new MaterialPageRoute<String>(
          fullscreenDialog: true,
          builder: (context) => new Scaffold (
            appBar: new AppBar(
              title: new Text (
                message,
                style:  new TextStyle(
                  fontFamily: "Exo",
                  fontWeight: FontWeight.w900,
                ),
              )
            ),
            body:         new Container (
            padding: new EdgeInsets.all(10.0),
            child: new Column(
              children: <Widget>[
                new Expanded(
                  child: new SWNSectorFileChooser(
                    files: _files,
                    onFileTap: (selectedFile) {
                      Navigator.pop(ctx,selectedFile);
                    },
                  ),
                ),                
                new Row (children: <Widget>[
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(5.0),
                      child: new RaisedButton(
                        child: new Text("Cancel"),
                        onPressed: () {
                          Navigator.pop(ctx,null);
                        },
                      ),
                    )
                  ),
                ],
                )
              ],
            ),
        )
      ),
    ),
  );
  return res;
}


