/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';
import 'dart:collection';

class SWNEditWorldData {
  String name;
  String atmosphere;
  String climate;
  String biosphere;
  String population;
  String technology;
  String tag1;
  String tag2;
  String origin;
  String contactPoint;
  String currentRelation;
  bool hasRelation;
  SWNEditWorldData (SWNWorld world) {
    name = world.name;
    atmosphere = world.atmosphere;
    climate = world.climate;
    biosphere = world.biosphere;
    population = world.population;
    technology = world.technology;
    tag1 = world.tag1.title;
    tag2 = world.tag2.title;
    hasRelation = world.relation != null;
    if (world.relation != null) {
      origin = world.relation.origin;
      contactPoint = world.relation.contactPoint;
      currentRelation = world.relation.currentRelation;
    }
  }
  @override
  toString() => "[ $name $atmosphere $biosphere $climate $population $technology $tag1 $tag2 ]";
}

typedef OKPressed (SWNEditWorldData data);
typedef CancelPressed ();

var doc = docWorldRelations["Relations"];
List<String> origins = doc.toList().where((f) => f != null).map( (f) => f["Origin"]).toList().cast<String>();
List<String> currentRelations = doc.toList().where((f) => f != null).map( (f) => f["CurrentRelation"]).toList().cast<String>();
List<String> contactPoints = doc.toList().where((f) => f != null).map( (f) => f["ContactPoint"]).toList().cast<String>();


class SWNWorldEditor extends StatefulWidget {
  SWNWorldEditor({this.world, this.onOkPressed, this.onCancelPressed});
  @override createState() => new SWNWorldEditorState(world);
  final SWNWorld world;
  final OKPressed onOkPressed;
  final CancelPressed onCancelPressed;
}

class SWNWorldEditorState extends State<SWNWorldEditor> {
  final SWNWorld world;
  SWNEditWorldData worldData;
  TextEditingController controller = new TextEditingController();

  SWNWorldEditorState(this.world) : super() {
    worldData = new SWNEditWorldData(world);
    controller.text = worldData.name;
    controller.addListener( () {
      setState( () {
        worldData.name = controller.text;
      });
    });
  }

  @override 
  Widget build(BuildContext context) {
    List<dynamic> tl = tagList;
    var tl2 = tl.map ( (f)  => f.toString()).toList();
    List<Widget> mainTabWidgets = [
      new Text("Name", textAlign: TextAlign.left),
      new Row (
        children: <Widget>[
          new Expanded (
            child: new TextField (controller: controller,),
          ),
          new IconButton(
            icon: new Icon(Icons.create),
            onPressed: () {
              controller.text = nameBuilder.createName();
            },
          )
        ],
      ),
      new Text("Tag 1", textAlign: TextAlign.left),
      new DropdownButton (
        value: worldData.tag1,
        items: tl2.map( (s) => new DropdownMenuItem<String>(
          key: new Key(s),
          value: s,
          child: new Text(s),
        )).toList(),
        key:  new Key(worldData.tag1),
        onChanged: ( (s) {
          setState( () {
            worldData.tag1 = s;
          });
        }), 
      ),
      new Text("Tag 2", textAlign: TextAlign.left),
      new DropdownButton (
        value: worldData.tag2,
        items: tl2.map( (s) => new DropdownMenuItem<String>(
          key: new Key(s),
          value: s,
          child: new Text(s),
        )).toList(),
        key:  new Key(worldData.tag2),
        onChanged: ( (s) {
          setState( () {
            worldData.tag2 = s;
          });
        }), 
      ),
    ];
    
    List<Widget> statsWidget = [
      new Text("Atmosphere", textAlign: TextAlign.left),
      new DropdownButton (        
        value: worldData.atmosphere,
        items: getTableLabels("Atmosphere").map( (s) => new DropdownMenuItem<String>(
          value: s,
          child: new Text(s),
        )).toList(),
        key:  new Key(worldData.atmosphere),
        onChanged: ( (s) {
          setState( () {
            print ("Setting $s");
            worldData.atmosphere = s;
          });
        }), 
      ),
      new Text("Biosphere", textAlign: TextAlign.left),
      new DropdownButton (
        value: worldData.biosphere,
        items: getTableLabels("Biosphere").map( (s) => new DropdownMenuItem<String>(
          key: new Key(s),
          value: s,
          child: new Text(s),
        )).toList(),
        key:  new Key(worldData.biosphere),
        onChanged: ( (s) {
          setState( () {
            worldData.biosphere = s;
          });
        }), 
      ),
      new Text("Climate", textAlign: TextAlign.left),
      new DropdownButton (
        value: worldData.climate,
        items: getTableLabels("Climate").map( (s) => new DropdownMenuItem<String>(
          key: new Key(s),
          value: s,
          child: new Text(s),
        )).toList(),
        key:  new Key(worldData.climate),
        onChanged: ( (s) {
          setState( () {
            worldData.climate = s;
          });
        }), 
      ),
      new Text("Population", textAlign: TextAlign.left),
      new DropdownButton (
        value: worldData.population,
        items: getTableLabels("Population").map( (s) => new DropdownMenuItem<String>(
          key: new Key(s),
          value: s,
          child: new Text(s),
        )).toList(),
        key:  new Key(worldData.population),
        onChanged: ( (s) {
          setState( () {
            worldData.population = s;
          });
        }), 
      ),
      new Text("Technology", textAlign: TextAlign.left),
      new DropdownButton (
        value: worldData.technology,
        items: getTableLabels("Technology").map( (s) => new DropdownMenuItem<String>(
          key: new Key(s),
          value: s,
          child: new Text(s),
        )).toList(),
        key:  new Key(worldData.technology),
        onChanged: ( (s) {
          setState( () {
            worldData.technology = s;
          });
        }), 
      ),
    ];

    List<Widget> relationWidgets = [];
    print (origins.runtimeType);
    if (world.relation != null) {
      relationWidgets.addAll( <Widget>[
        new Text("Origin", textAlign: TextAlign.left),
        new DropdownButton (        
          value: worldData.origin,
          items: origins.map( (s) => new DropdownMenuItem<String>(
            value: s,
            child: new Text(s),
          )).toList(),
          key:  new Key(worldData.origin),
          onChanged: ( (s) {
            setState( () {
              print ("Setting $s");
              worldData.origin = s;
            });
          }), 
        ),
        new Text("Current Relation", textAlign: TextAlign.left),
        new DropdownButton (
          value: worldData.currentRelation,
          items: currentRelations.map( (s) => new DropdownMenuItem<String>(
            key: new Key(s),
            value: s,
            child: new Text(s),
          )).toList(),
          key:  new Key(worldData.currentRelation),
          onChanged: ( (s) {
            setState( () {
              worldData.currentRelation = s;
            });
          }), 
        ),
        new Text("Contact Point", textAlign: TextAlign.left),
        new DropdownButton (
          value: worldData.contactPoint,
          items: contactPoints.map( (s) => new DropdownMenuItem<String>(
            key: new Key(s),
            value: s,
            child: new Text(s),
          )).toList(),
          key:  new Key(worldData.contactPoint),
          onChanged: ( (s) {
            setState( () {
              worldData.contactPoint = s;
            });
          }), 
        )
      ]);      
    }
    else {
      relationWidgets.add(new Text ("No relations defined for world "+ world.name));
    }

    var buttonRow = new Row (children: <Widget>[
          new Expanded(
            child: new Container (
               padding: new EdgeInsets.all(4.0),
                child: new RaisedButton(
                  child: new Text("Ok"),
                  onPressed: () {
                    if (widget.onOkPressed != null)
                      widget.onOkPressed(worldData);
                  },
              ),
            ),
          ),
          new Expanded(
            child: new Container (
               padding: new EdgeInsets.all(4.0),
                child: new RaisedButton(
                  child: new Text("Create new"),
                  onPressed: () {
                    setState(() {
                      var wNew = new SWNWorld.randomize();
                      if (worldData.hasRelation)
                        wNew.relation = new SWNRelation.randomize();
                      worldData = new SWNEditWorldData(wNew);
                      controller.text = worldData.name;
                    });
                  },
              ),
            ),
          ),
          new Expanded(
            child: new Container (
                padding: new EdgeInsets.all(4.0),
                child: new RaisedButton(
                  child: new Text("Cancel"),
                  onPressed: () {
                    if (widget.onCancelPressed != null)
                      widget.onCancelPressed();
                  },
                ),
              ),
            ),
        ]
      );

    var tabController = new DefaultTabController(
      length: 3,
      child: new Column( children: <Widget>[
        new TabBar(
          tabs: <Widget>[
            new Container( child: new Text("Main infos"), padding: new EdgeInsets.all(10.0)),
            new Container( child: new Text("Stats"),padding: new EdgeInsets.all(10.0)),
            new Container( child: new Text("Relations"),padding: new EdgeInsets.all(10.0))
          ],
        ),
        new Expanded (
          child: new TabBarView(
            children: <Widget>[
              new Container( child: new Column (              
                crossAxisAlignment: CrossAxisAlignment.start,            
                mainAxisAlignment: MainAxisAlignment.start,
                children: mainTabWidgets
              ), padding: new EdgeInsets.all(10.0) ,),
              new Container( child: new Column (              
                crossAxisAlignment: CrossAxisAlignment.start,            
                mainAxisAlignment: MainAxisAlignment.start,
                children: statsWidget
              ), padding: new EdgeInsets.all(10.0) ,),
              new Container( child: new Column (  
                crossAxisAlignment: CrossAxisAlignment.start,            
                mainAxisAlignment: MainAxisAlignment.start,
                children: relationWidgets
              ), padding: new EdgeInsets.all(10.0) ,),
            ],
          ),
        ),
      ]
    )
    );  
    
    var mainView = new Column(
      children: <Widget>[
        new Expanded(
          child: tabController,        
        ),
        buttonRow
      ],
    );
 
    return mainView;
  }
}