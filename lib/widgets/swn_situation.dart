/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';

typedef void SWNSituationCallback(SWNSituation situation);


class SWNSituationWidget extends StatefulWidget {
  final SWNSituation situation;
  final SWNSituationCallback onRemoveSituationPressed;
  SWNSituationWidget ({ this.situation, this.onRemoveSituationPressed});
  @override
  createState() => new SWNSituationWidgetState();
}

class SWNSituationWidgetState extends State<SWNSituationWidget> {

  @override
  Widget build (BuildContext context) {
    SWNSituation situation = widget.situation;
    return new Container (
      padding: const EdgeInsets.all(2.0),
      child: new Card(
        child: new Column(
          children: <Widget>[
            new ListTile(
              title: new Text(situation.location),
              subtitle: new Text ("Situation: ${situation.situation}. Occupied by: ${situation.occupiedBy}."),
            ),
            new Container (
              padding: new EdgeInsets.fromLTRB(0.0, 6.0, 0.0, 6.0),
              alignment: Alignment.centerRight,
              child: new IconButton (
                splashColor: Colors.green.shade100,
                /*child: new Text("Remove from system"),*/
                icon: new Icon(Icons.delete),
                onPressed: (){
                    print ("!");
                    if (widget.onRemoveSituationPressed != null)
                      widget.onRemoveSituationPressed(widget.situation);
                }),
              )
          ],
        )
      ),
    );
  }   
}

class SWNRelationWidget extends StatelessWidget {
  final SWNRelation relation;

  SWNRelationWidget(this.relation);
  @override
  Widget build (BuildContext context) {
    return new Container (
      padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 16.0),
        child: new ListTile(
          title: new Text("Relation with primary:"),
          subtitle: new Text (relation.toString()),
      ),
    );
  }   
}

