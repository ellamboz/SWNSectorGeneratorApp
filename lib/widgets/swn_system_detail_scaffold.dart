/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';
import 'swn_system_detail.dart';
import 'swn_dialogs.dart' as Dialogs;

enum SWNSystemDetailMenu {
  AddNewWorld,
  AddNewSituation,
  RenameSystem
}


class SWNSystemDetailScaffold extends StatefulWidget {
  final SWNSector sector;
  final SWNSystem system;
  final BuildContext ctx;
  SWNSystemDetailScaffold({this.sector, this.system, this.ctx});
  @override 
  createState() => new SWNSystemDetailScaffoldState();
}

class SWNSystemDetailScaffoldState extends State<SWNSystemDetailScaffold> {
  BuildContext contextInner;

  @override 
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Builder (
      builder: (context) => new Scaffold (
        appBar: new AppBar(              
          title: new Text (
            widget.system.name+" system",
            style:  new TextStyle(
              fontFamily: "Exo",
              fontWeight: FontWeight.w900,
            ),
          ),
          actions: <Widget>[
            _detailMenuActions(widget.ctx, widget.system)
          ],
        ),
        body: new Builder( 
          builder: (BuildContext ctxInner) { 
            contextInner = ctxInner;
            return new SWNSystemDetail(widget.system);
          }
          ),
        ),
      );    
  }

  PopupMenuButton<SWNSystemDetailMenu> _detailMenuActions(BuildContext ctx, SWNSystem system) {
    return new PopupMenuButton<SWNSystemDetailMenu> (
      itemBuilder: (BuildContext ctx2) => <PopupMenuItem<SWNSystemDetailMenu>>[
        const PopupMenuItem<SWNSystemDetailMenu>(
          value: SWNSystemDetailMenu.AddNewWorld,
          child: const Text('Add a new world')
        ),
        const PopupMenuItem<SWNSystemDetailMenu>(
          value: SWNSystemDetailMenu.AddNewSituation,
          child: const Text('Add a new situation')
        ),
        const PopupMenuItem<SWNSystemDetailMenu>(
          value: SWNSystemDetailMenu.RenameSystem,
          child: const Text('Rename system')
        ),
      ],
      onSelected: (SWNSystemDetailMenu value) {
        switch (value) {
          case SWNSystemDetailMenu.AddNewWorld:
            _detailAddNewWorld(system);
          break;
          case SWNSystemDetailMenu.AddNewSituation:
            _detailAddNewSituation(system);
          break;
          case SWNSystemDetailMenu.RenameSystem:
            _detailEditSystemName(ctx, system);
          break;
        }
      },
    );
  }

  void _detailAddNewWorld (SWNSystem system) {
    setState( ()  {
      if (system.worlds.length < 4) {
        var w = new SWNWorld.randomize();
        w.relation = new SWNRelation.randomize(); 
        system.addWorld(w);
        //print ("currentState: ${scaffoldKey.currentState}");
        Scaffold.of(contextInner).showSnackBar(
            new SnackBar(
            backgroundColor: Colors.green.shade900,
            content: new Text("Added world '${w.name}' to current system"),
          )
        );
      }
    });    
  }

  void _detailAddNewSituation (SWNSystem system) {
    setState( ()  {
      if (system.situations.length < 6)
        system.situations.add(new SWNSituation());
        Scaffold.of(contextInner).showSnackBar(
          new SnackBar(
            backgroundColor: Colors.green.shade900,
            content: new Text("Added situation  to current system"),
          )
        );
    });
    
  }

  void _detailEditSystemName (BuildContext ctx, SWNSystem system) {
    Dialogs.inputBox("Enter new system name", system.name, ctx)
    .then( (newName) {
      if (newName != null)
      setState( () {
        system.name = newName;
      });
    }); 
  }

  
}