/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';
import 'swn_common_data.dart';

class SWNTagWidget extends StatelessWidget {
  final SWNTag tag;
  final TextStyle titleStyle = new TextStyle (
    fontWeight: FontWeight.bold,
    fontSize: 14.0
  );
  final TextStyle titleStyle2 = new TextStyle (
    fontWeight: FontWeight.bold,
    fontSize: 18.0
  );
  SWNTagWidget(this.tag);


  @override
  Widget build (BuildContext context) {
    return new Container (
      padding: const EdgeInsets.all(16.0),
      child: new Column (
        children: <Widget>[
          expandedBox (new Text (tag.title, style: titleStyle2)),
          new Text (tag.description, textAlign: TextAlign.left),
          expandedBox (new Text ("Enemies", style: titleStyle, textAlign: TextAlign.left,)),
          new Text (tag.enemies, textAlign: TextAlign.left),
          expandedBox (new Text ("Friends", style: titleStyle, textAlign: TextAlign.left)),
          new Text (tag.friends, textAlign: TextAlign.left),
          expandedBox (new Text ("Complications", style: titleStyle, textAlign: TextAlign.left)),
          new Text (tag.complications, textAlign: TextAlign.left),
          expandedBox (new Text ("Places", style: titleStyle, textAlign: TextAlign.left)),
          new Text (tag.places, textAlign: TextAlign.left),
          
        ],
      ),
    );
  }
}

