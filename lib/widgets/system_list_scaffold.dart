/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';

typedef void OnTapDelegate(SWNCoord coord, BuildContext ctx);

class SystemListScaffold extends StatefulWidget {
  final SWNSector sector;
  final BuildContext callingCtx;
  final OnTapDelegate onTap;
  SystemListScaffold({this.sector, this.callingCtx, this.onTap});
  @override
  State<StatefulWidget> createState() {
      // TODO: implement createState
      return new SystemListScaffoldState();
    }
}

class SystemListScaffoldState extends State<SystemListScaffold> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var coords = widget.sector.systems.keys.toList();
    coords.sort();
    var items = coords.map( (c) => new ListTile(
      dense: true,
      title: new Text("$c - ${widget.sector.systems[c].name}"),
      subtitle: new Text("${widget.sector.systems[c].worlds.length} worlds, ${widget.sector.systems[c].situations.length} situations"),
      onTap: () {
        if (widget.onTap != null) {
          widget.onTap(c, widget.callingCtx);
        }
      },
    )).toList();
    var ib =  new Scaffold (
        appBar: new AppBar(              
          title: new Text (
            "${widget.sector.name} systems",
            style:  new TextStyle(
              fontFamily: "Exo",
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
        body: new ListView (                
          children: items,
        ),
      );
    return ib;    
  }
}