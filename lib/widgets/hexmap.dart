/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
import '../swn.dart';
import 'dart:math';
import 'dart:ui' as UI;
import 'dart:typed_data';

typedef void CoordCallback(SWNCoord coord) ;

class CanvasSize {
  Canvas canvas;
  Size size;
  CanvasSize (this.canvas, this.size);
}

Random rnd = new Random();
//final backgroundImg = new 
final backgroundRect = new Rect.fromLTWH(0.0,0.0,582.0, 1168.0);

class HexmapSectorPainter extends CustomPainter {
  HexmapSectorPainter({
    this.sector,
    this.zoom,
    this.offset,
    this.forward,
    this.context,
    this.bkImage
  }):super(){
  }

  final UI.Image bkImage;

  SWNSector sector;
  final double zoom;
  final Offset offset;
  final bool forward;
  final BuildContext context;
  double fontSize = 10.0;
  double rectSize = 100.0;
  
  final Color boxLineColor = Colors.yellow;
  final Color boxBackgroundColor = Colors.deepPurple.shade900;
  final Color textColor = Colors.white;
  final Color skyColor = Colors.blueGrey.shade900;

  final RadialGradient skyGradient = new RadialGradient(
    center: const Alignment(-0.7, -0.7),
    radius: 1.0,
    colors: [
      const Color(0xFF000040),
      const Color(0xFF0000AF),
    ],
    stops: [0.0, 1.0]
  );

  final RadialGradient skyGradient2 = new RadialGradient(
    center: const Alignment(-0.7, -0.7),
    radius: 1.0,
    colors: [
      const Color(0xFF0000FF),
      const Color(0xFF000070),
    ],
    stops: [0.0, 1.0]
  );

  void _paintSquare (Canvas canvas, Offset offset, Size size, SWNCoord coord, double textSize)  {
    String label = sector.systems.containsKey(coord) ? coord.toString()+" "+sector.systems[coord].name : coord.toString();
    Offset textBase = new Offset( 4.0*zoom, size.height - (4.0*zoom+textSize*1.1)); 
    
    final Rect rect = new Rect.fromPoints(offset, new Offset(offset.dx+size.width, offset.dy+size.height) );


    final Paint paint2 = new Paint()
      ..strokeWidth = 2.0 * zoom
      ..color = boxLineColor
      ..style = PaintingStyle.stroke;
    final Paint paint = new Paint()
      ..shader = skyGradient.createShader(rect)
      ..style = PaintingStyle.fill;
      

    canvas.drawRect(rect, paint);    
    canvas.drawRect(rect, paint2);  

    _drawPlanetsInSquare(canvas, offset, size, coord);


    TextSpan span = new TextSpan(style: new TextStyle(fontFamily: "Encode", fontSize: textSize, color: textColor), text: label);
    TextPainter tp = new TextPainter(text: span, textAlign: TextAlign.center, textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(canvas, offset+textBase);  
  }



  void _drawPlanetsInSquare (Canvas canvas, Offset offset, Size size, SWNCoord coord) {
    if (sector.systems.containsKey(coord)) {
      //srprint ("Drawing on  coord: $coord");
      var system = sector.systems[coord];
      double planetSize = (size.width * 0.1 );
      if (system.worlds.length < 2) {
        Offset dc = new Offset(( size.width/2.0), (size.height / 2.0));
        Offset center = offset + dc;
        var paint3 = new Paint()
        ..color = Colors.green.shade100
        ..style = PaintingStyle.fill;
        canvas.drawCircle(center, planetSize, paint3);
      }
      else {
        Offset dc = new Offset(( size.width/2.0)+size.width*0.05, (size.height / 2.0)-size.height*0.05);
        Offset center = offset + dc;
        var paint3 = new Paint()
        ..color = Colors.green.shade700
        ..style = PaintingStyle.fill;
        canvas.drawCircle(center, planetSize, paint3);
        dc = new Offset(( size.width/2.0)-size.width*0.05, (size.height / 2.0)+size.height*0.05);
        center = offset + dc;
        paint3 = new Paint()
        ..color = Colors.green.shade100
        ..style = PaintingStyle.fill;
        canvas.drawCircle(center, planetSize, paint3);
      }
      _drawSituationsInSquare (canvas, offset, size, system);
      
    }
    
  }
  
  void _drawSituationsInSquare(Canvas canvas, Offset offset, Size size, SWNSystem system) {
    double squareSize = 10.0 * zoom;
    for (int x = 1; x <= system.situations.length ; x++) {
      final Offset ox = offset+new Offset (x.toDouble() * (squareSize+2.0)-4.00*zoom, 6.0*zoom);
      final Offset oy = new Offset (ox.dx+squareSize, ox.dy+squareSize);
      final Rect rect = new Rect.fromPoints(ox,oy);
      var paint3 = new Paint()
      ..color = Colors.green.shade700
      ..style = PaintingStyle.fill;
      canvas.drawRect(rect, paint3);
    }
  }

  Rect curRect;

  @override
  void paint(Canvas canvas, Size size)  {
    doPaint(new CanvasSize(canvas, size));
  }

  void doPaint(CanvasSize cs) {
    var canvas = cs.canvas;
    var size = cs.size;
    double _newRectSize = rectSize*zoom;
    double _stepSize = _newRectSize * 1.1;
    double _newfontSize =  fontSize*zoom;
    

    curRect = new Rect.fromLTWH(0.0, 0.0, size.width, size.height);

    if(bkImage != null)
      canvas.drawImageRect (
        bkImage, 
        new Rect.fromLTWH(0.0, 0.0, bkImage.width.toDouble(), bkImage.height.toDouble()),
        curRect,
        new Paint());

    for (int x = 0; x < 8; x++) {
      for (int y = 0; y < 10; y++) {
        var coord = new SWNCoord(x, y);
        double dx = x.toDouble() * _stepSize;
        double dy = ( x % 2 ==1 ) ? y.toDouble() * _stepSize + _stepSize / 2.0 : y.toDouble() * _stepSize;
        final Offset pos = new Offset(dx,dy)+offset;
        final Size siz = new Size(_newRectSize, _newRectSize);
        final Rect squareRect = new Rect.fromPoints(pos, new Offset(pos.dx+siz.width, pos.dy+siz.height));
        final Rect inter = curRect.intersect(squareRect);

        if (inter.width >= 0.0 && inter.height >= 0.0) {
          String label="";
          if (sector.systems.containsKey(coord)){  
            label = coord.toString()+" "+ sector.systems[coord].name;
            _paintSquare(canvas, pos, siz,coord, _newfontSize* 1.1);

          }
          else {
            label = coord.toString();
            _paintSquare(canvas, pos, siz,coord, _newfontSize);
          }
        }
      }
    }

  }

  @override
  bool shouldRepaint(HexmapSectorPainter oldPainter) {
    return oldPainter.zoom != zoom
        || oldPainter.offset != offset
        || oldPainter.forward != forward;
  }

}

class HexmapSector extends StatefulWidget {
  HexmapSector({
    this.sector,
    this.tapWithCoord,
    this.longTapWithCoord
  }) :super() {
    //print ("Recreating... ${sector.systems.length}");
    
  }

  final CoordCallback tapWithCoord;
  final CoordCallback longTapWithCoord;
  final SWNSector sector;
  @override
  HexmapSectorState createState() => new HexmapSectorState();
}

class HexmapSectorState extends State<HexmapSector> {

  HexmapSectorState():super() {
    
    rootBundle.load("assets/galaxy.jpg").then( (bd) {
      Uint8List lst = new Uint8List.view(bd.buffer);
      UI.instantiateImageCodec(lst).then( (codec) {
        codec.getNextFrame().then(
          (frameInfo) {
            bkImage = frameInfo.image;
            print ("bkImage instantiated: $bkImage");
            setState( () {
              _zoom = _zoom - 0.0001;
            });
          }
        );
      });
    });
        
  }

  UI.Image bkImage;

  Offset _startingFocalPoint;

  Offset _previousOffset;
  Offset _offset = Offset.zero;
  Offset _lastTapped;

  Offset get lastTapped  => _lastTapped;
  
  SWNCoord get coord {
    int dx =  (_lastTapped.dx / 110.0 / _zoom).truncate();
    int dy =  ((_lastTapped.dy- (dx % 2 == 1 ?   55.0*_zoom : 0.0 )) /110.0/ _zoom   ).truncate();
    return new SWNCoord(dx,dy);
  }

  double _previousZoom;
  double _zoom = 1.0 + rnd.nextDouble() / 10000.0;

  bool _forward = true;
  bool _scaleEnabled = true;
  bool _doubleTapEnabled = true;
  bool _longPressEnabled = true;

  CoordCallback get tapWithCoord => widget.tapWithCoord;
  CoordCallback get longTapWithCoord => widget.longTapWithCoord;

  void _handleScaleStart(ScaleStartDetails details) {
    setState(() {
      _startingFocalPoint = details.focalPoint;
      _previousOffset = _offset;
      _previousZoom = _zoom;
    });
  }

  void resetState() {
    setState( () {
      _zoom = _zoom;
    });
  }

  void _handleScaleUpdate(ScaleUpdateDetails details) {
    setState(() {
      _zoom = _previousZoom * details.scale;
      //print ("Scale: $_zoom");
      // Ensure that item under the focal point stays in the same place despite zooming
      final Offset normalizedOffset = (_startingFocalPoint - _previousOffset) / _previousZoom;
      _offset = details.focalPoint - normalizedOffset * _zoom;
    });
  }

  void _handleScaleReset() {
    setState(() {
      _zoom = 1.0;
      _offset = Offset.zero;
    });
  }


  void _handleDirectionChange() {
    setState(() {
      _forward = !_forward;
    });
    if (longTapWithCoord != null)
      longTapWithCoord(coord);
  }

  void _handleTapDown (TapDownDetails details) {
    final RenderBox refBox = context.findRenderObject();
    _lastTapped = refBox.globalToLocal(details.globalPosition) - _offset;        
  }

  void _handleTap() {
    if (tapWithCoord != null)
      tapWithCoord(coord);
  }


  @override
  Widget build(BuildContext context) {
    double f = rnd.nextDouble() / 100000.0 - 1.0/200000.0;
    _zoom += f;
    //print ("Rebuilding HexmapSector --- $_zoom");

    var _customPaint = new CustomPaint(
            painter: new HexmapSectorPainter(
              zoom: _zoom,
              offset: _offset,
              forward: _forward,
              sector: widget.sector,
              context: context,
              bkImage: bkImage
            )
          ); 


    return new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        new GestureDetector(
          onScaleStart: _scaleEnabled ? _handleScaleStart : null,
          onScaleUpdate: _scaleEnabled ? _handleScaleUpdate : null,
          onTapDown: _handleTapDown,
          onTap: _handleTap,
          onDoubleTap: _doubleTapEnabled ? _handleScaleReset : null,
          onLongPress: _longPressEnabled ? _handleDirectionChange : null,
          child: _customPaint,       
        ),
      ]
    );
  }
}

