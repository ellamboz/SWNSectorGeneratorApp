/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import 'dart:async';
import '../swn.dart';
import 'dart:io';
import 'hexmap.dart';
import 'swn_dialogs.dart' as Dialogs;
import 'swn_file_chooser_dialog.dart' as FileChooser;
import 'hexmap_longtap_handler.dart';
import 'swn_system_detail_scaffold.dart';
import 'system_list_scaffold.dart';

typedef Widget BuildWidgetDelegate(BuildContext ctx);


enum SWNMainAppMenu {
  CreateNewSector,
  LoadSector,
  SaveSector,
  ShareSector,
  RenameSector,
  AboutDialog,
  ExitApp
}

final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();


class SWNMainScaffoldHexmap extends StatefulWidget{
  final SWNSector sector;
  SWNMainScaffoldHexmap({this.sector});
  @override
  State<StatefulWidget> createState() {
      // TODO: implement createState
      return new SWNMainScaffoldHexmapState(sector: this.sector);
    }

}

class SWNMainScaffoldHexmapState extends State<SWNMainScaffoldHexmap> {
  
  SWNMainScaffoldHexmapState({this.sector});


  SWNSector sector;
  BuildContext contextInner;

  HexmapSector _hexmap;  

  @override 
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Builder(
      builder: (ctx) {
        return new Scaffold(            
          key: scaffoldKey,
          appBar: new AppBar( 
            title: new Text ("${sector.name} Sector" ,
              style:  new TextStyle(
              fontFamily: "Exo",
              fontWeight: FontWeight.w900,
            )),
            actions: <Widget>[
              _menuSystemsList(ctx),
              _mainMenuActions(ctx) 
            ] ,
          ),
          body:  new Builder (
            builder: (ctx2) => _makeHexmapSector(ctx2)
          ),
        );
      }
    );
  }

  IconButton _menuSystemsList (BuildContext ctx) {
    var iconButtonListScaffold = new IconButton (
      icon: new Icon(Icons.list),
      onPressed: () {
        Navigator.of(ctx).push (
          new MaterialPageRoute(
            builder: (BuildContext context) =>
              new SystemListScaffold(
                sector: sector ,
                callingCtx:  ctx,
                onTap: (coord, ctxList) {
                  setState( () {
                    _handleTapWithCoord(coord, ctxList);
                  });
                },
              ),
            ),
          );
        },
      );
    return iconButtonListScaffold;     
  }

  void _handleTapWithCoord(SWNCoord coord, BuildContext ctx) {
    try {
      var nav = Navigator.of(ctx);
      //print ("Nav is: $nav");
      if (sector.systems.containsKey(coord)) {
        var system = sector.systems[coord];
        nav.push (
          new MaterialPageRoute(
          builder: (context) => new SWNSystemDetailScaffold(
            sector: this.sector,
            system: system,
            ctx: ctx
          ),
          ),
        );
      }
    }
    catch (e)
    {
      print ("Error: $e");
    }
  }
  

  HexmapSector _makeHexmapSector(BuildContext ctx) {
    //print ("Rebuilding hexmap...${sector.systems.length}");
    _hexmap = new  HexmapSector(
        sector: sector,
        tapWithCoord: (coord) {
          _handleTapWithCoord(coord,ctx);
        },
        longTapWithCoord: (coord)  {
          HexmapLongtapHandler ltHandler = new HexmapLongtapHandler(ctx, sector, coord);
          Future<LongTapOptions> lto = ltHandler.handleLongTapWithCoord();
          lto.then( (opts) {
            switch (opts) {
              case LongTapOptions.DeleteSystem:
                Dialogs.confirmationDialog("Delete underlying system?", ctx)
                .then( (b) {
                  if (b) setState(() {
                    print ("Delete system...");
                    sector.removeSystem(coord) ;
                    try {
                      //We're using original context here
                      scaffoldKey.currentState.showSnackBar(
                        new SnackBar(
                          backgroundColor: Colors.green.shade900,
                          content: new Text("System removed at coord '${coord}'"),
                        )
                      );
                    } catch (e) {
                      print (e);
                    }
                  });
                });
              break;
              case LongTapOptions.NewSystem:
                setState(() { 
                  sector.addSystem(coord);
                  print (Scaffold.of(ctx));                  
                  scaffoldKey.currentState.showSnackBar(
                    new SnackBar(
                      backgroundColor: Colors.green.shade900,
                      content: new Text("System added at coord '${coord}'"),
                    )
                  );
                });
              break;                                       
              case LongTapOptions.AddWorld:
                setState(() { 
                  sector.addWorld(coord);
                  scaffoldKey.currentState.showSnackBar(
                    new SnackBar(
                      backgroundColor: Colors.green.shade900,
                      content: new Text("World added on system at coord '${coord}'"),
                    )
                  );
                });
              break;                                       
              case LongTapOptions.AddSituation:
                setState(() { 
                  sector.addSituation(coord);
                  scaffoldKey.currentState.showSnackBar(
                    new SnackBar(
                      backgroundColor: Colors.green.shade900,
                      content: new Text("Situation added on system at coord '${coord}'"),
                    )
                  );
                });
              break;  
              default:
            }
            setState( () {
              print ("NNNAME system...");
              sector.systems[sector.systems.keys.first].name += "";
            });
          });
        }
      );
    return _hexmap;
  }  

  void _createNewSector(BuildContext ctx) {
    Dialogs.confirmationDialog("Create a new sector?", ctx)
    .then( (b) {
      if (b) setState(() {
        sector = new SWNSector();
        scaffoldKey.currentState.showSnackBar(
          new SnackBar(
            backgroundColor: Colors.green.shade900,
            content: new Text("New sector created."),
          )
        );
      });
    });
  }

  Future<Null> _saveSector (BuildContext ctx) async {
    bool saveFile = await Dialogs.confirmationDialog("Save the sector "+sector.name+" ?", ctx);
    if (saveFile) {
      final dir = await getApplicationJsonPathDir();      
      bool bDirExists = await dir.exists();
      if ( bDirExists ) 
        await dir.create();
      final file = new File("${dir.path}/${sector.name}.swnj");
      await this.sector.toJsonFile(file.path);
    }
  }

  Future<String> _shareSector (BuildContext ctx) async {
    final dir = await getApplicationHtmlPathDir();      
    bool bDirExists = await dir.exists();
    if ( bDirExists ) 
      await dir.create();
    final file = new File("${dir.path}/${sector.name}.html");
    print ("Sharing $file");
    await this.sector.toHtmlFile(file.path);
    print ("About to share... $file");
    String gu = await shareFile(sector.name, file.path);
    return gu;
  }

  Future <bool> _loadSector (BuildContext ctx) async {
    String filePath = await FileChooser.fileDialog("Load a saved sector", "swnj", ctx);
    print ("About to load $filePath");
    if (filePath != null) {
      var newSector = await SWNSector.fromJsonFile(filePath);
      sector = newSector;
      return true;
    }
    else 
      return false;
  }


  PopupMenuButton<SWNMainAppMenu> _mainMenuActions(BuildContext ctx) {
    return  new PopupMenuButton<SWNMainAppMenu>(
    onSelected: (SWNMainAppMenu value) {
      switch (value) {
        case SWNMainAppMenu.CreateNewSector:
          _createNewSector(ctx);
          break;
        case SWNMainAppMenu.LoadSector:
          _mainMenuLoadSector(ctx);
        break;
        case SWNMainAppMenu.RenameSector:
          _mainMenuRenameSector(ctx);
        break;
        case SWNMainAppMenu.SaveSector:
          _mainMenuSaveSector(ctx);
        break;
        case SWNMainAppMenu.ShareSector:
          _mainMenuShareSector(ctx);
        break;
        case SWNMainAppMenu.AboutDialog:
          showAboutDialog(
            context:  ctx,
            applicationName: "SWN Mobile Sectors",
            applicationVersion: "0.0.3"
          );
        break;
        case SWNMainAppMenu.ExitApp:
          Dialogs.confirmationDialog("Do you want to exit?", ctx)
          .then( (b) {
            exit(0);
          });
        break;
        default:
        break;
      };
    },
    itemBuilder: (BuildContext context) => _mainMenuItemBuilder(),
    );
  }

  List<PopupMenuEntry<SWNMainAppMenu>> _mainMenuItemBuilder() => <PopupMenuEntry<SWNMainAppMenu>>[
      const PopupMenuItem<SWNMainAppMenu>(
        value: SWNMainAppMenu.CreateNewSector,
        child: const Text('Create a new sector')
      ),
      const PopupMenuItem<SWNMainAppMenu>(
        value: SWNMainAppMenu.RenameSector,
        child: const Text('Rename this sector')
      ),
      const PopupMenuDivider(),
      const PopupMenuItem<SWNMainAppMenu>(
        value: SWNMainAppMenu.LoadSector,
        child: const Text('Load an existing sector')
      ),
      const PopupMenuItem<SWNMainAppMenu>(
        value: SWNMainAppMenu.SaveSector,
        child: const Text('Save this sector')
      ),
      const PopupMenuItem<SWNMainAppMenu>(
        value: SWNMainAppMenu.ShareSector,
        child: const Text('Share this sector')
      ),
      const PopupMenuDivider(),
      const PopupMenuItem<SWNMainAppMenu>(
        value: SWNMainAppMenu.AboutDialog,
        child: const Text('About this app')
      ),
      const PopupMenuItem<SWNMainAppMenu>(
        value: SWNMainAppMenu.ExitApp,
        child: const Text('Exit the app')
      ),
    ];



  void _mainMenuLoadSector(BuildContext ctx) {
    _loadSector(ctx)
    .then ( (v) {
      if (v) {
        setState( () {
          scaffoldKey.currentState.showSnackBar(
            new SnackBar(
              backgroundColor: Colors.green.shade900,
              content: new Text("Sector ${sector.name} loaded."),
            )
          );
        });
      }
    }); 
  }

  void _mainMenuRenameSector(BuildContext ctx) {
    Dialogs.inputBox("Enter new sector name", sector.name, ctx)
    .then( (newName) {
      if (newName != null)
      setState( () {
        sector.name = newName;

        
      });
    }); 
  }

  void _mainMenuSaveSector(BuildContext ctx) {
    _saveSector(ctx)
    .then( (v) {
      setState( () {
        scaffoldKey.currentState.showSnackBar(
          new SnackBar(
            backgroundColor: Colors.green.shade900,
            content: new Text("Sector ${sector.name} saved."),
          )
        );

      });
    });    
  }

  void _mainMenuShareSector(BuildContext ctx) {
    _shareSector(ctx)
    .then( (v) {
      print ("share result... $v");
      setState( () {
        scaffoldKey.currentState.showSnackBar(
          new SnackBar(
            backgroundColor: Colors.green.shade900,
            content: new Text("Sector ${sector.name} shared."),
          )
        );

      });
    });    
  }
  


}