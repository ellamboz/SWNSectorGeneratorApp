/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'dart:math';
import 'swn_data.dart' as SWN;

var swnTags = SWN.docSwnWorldTags["Tags"];
var swnSituations = SWN.docSwnSituations["Situations"];

var c = "bcdfgklmnprstvz";
var v = "aeiou";

List<String> s1 = [];
List<String> s2 = [];

String word() {

  int nSyllables = 2 + rnd.nextInt(4);
  int nStart = rnd.nextInt(1000);
  String sRes = "";
  bool hasSplit = false;

  if (s1.isEmpty && s2.isEmpty) {
      c.runes.forEach((int rune) {
      var character=new String.fromCharCode(rune);
      v.runes.forEach ((int rune2) {
        var c1 = new String.fromCharCode(rune2);
        s1.add(character + c1);
        s2.add(c1+character );
      });
    });
  }
  
  for (var h = 0; h < nSyllables; h++) {
    var syl = nStart % 2 == 0 ? s1[rnd.nextInt(s1.length)] : s2[rnd.nextInt(s2.length)];
    if (nStart % 2 == 0 && rnd.nextInt(6)==2)
      syl+="h";
    if (nSyllables > 3 && h > 1 && rnd.nextInt(5) == 2 && !hasSplit)
    {
	    sRes += " "+ syl;
      hasSplit = true;
    }
    else
	    sRes += syl;
    nStart += rnd.nextInt(4) == 2 ? 1 : 2;
  }
  
  // linguistic drift
  if (rnd.nextInt(4) == 3) sRes = sRes.replaceFirst("oo","oa");
  if (rnd.nextInt(4) == 3) sRes = sRes.replaceFirst("uu","ue");
  if (rnd.nextInt(4) == 3) sRes = sRes.replaceFirst("c","ch");
  if (rnd.nextInt(4) == 3) sRes = sRes.replaceFirst("s","sh");
  if (rnd.nextInt(4) == 3) sRes = sRes.replaceFirst("z","tz");
  
  return sRes;
}


Random rnd = new Random();

int d6() => rnd.nextInt(6)+1;
int d8() => rnd.nextInt(8)+1;
int d10() => rnd.nextInt(10)+1;
int Roll2d6() => d6() + d6();



Map<String, String> getSituation() {
  var obj = swnSituations[d8()-1];
  var location = obj["Description"];
  var x1 = d10();
  var x2 = d10();
  var situation = obj["Table"]["Situation"].where ( (s) => x1 >= s["Roll"] && x1 <= s["RollMax"]  ).first["Description"];
  var occupiedBy = obj["Table"]["Occupied By"].where ( (s) => x2 >= s["Roll"] && x2 <= s["RollMax"]  ).first["Description"];
  return {
    "Location" : location,
    "Situation" : situation,
    "Occupied By" : occupiedBy
  };
}

String getTable(String table) {
  var x1 = d6()+d6();
  var tab = SWN.docSwnWorldTables[table];
  var res = tab.where ( (s) => x1 >= s["Roll"] && x1 <= s["RollMax"]  ).first["Description"];
  return res;
}

String getTableIdx(String table, int idx) {
  var x1 = idx;
  var tab = SWN.docSwnWorldTables[table];
  var res = tab.where ( (s) => x1 >= s["Roll"] && x1 <= s["RollMax"]  ).first["Description"];
  return res;
}


