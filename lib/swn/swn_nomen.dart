/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'dart:math' as DM;
import 'swn_data.dart' as DATA;

final DM.Random random = new DM.Random();
String randomItem(List<String> l) => l[random.nextInt(l.length)];

String capitalize(String str) {
  return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
}



class NameBuilder {
  Map<String,int> vf = {"a": 2016,"à": 100,
                        "e": 3129,"é": 150,"è": 150, 
                        "i": 1679,                         
                        "u": 297, 
                        "o": 1922, 
                        "y": 257};
	Map<String,int> cf = {"t": 1556, "ch": 224, "p": 627, "r": 1518, "j": 158, "c": 730, 
                        "g": 243, "n": 1704, "b": 216, "k": 27, "f": 274, "v": 333, 
                        "d": 847, "l": 1160, "s": 1885, "sh": 311, "m": 705,
                        "z": 22, "zh": 140};
  
  int vfTot = 0;
  int cfTot = 0;

  String _lastCl = "";
  String _lastCV = "";
  String _lastVC = "";
  String _lastCVC = "";
  String _lastVCV = "";
  
  String v() {
    int x = random.nextInt(vfTot);
    int h = 0;
    var lk = vf.keys.toList();
    lk.sort();
	//print (lk);
    int y = 0;
	  String c = "";
    do {
	  //print ("$vfTot $y");
      y+= vf[lk[h]];
	  c = lk[h];
      h++;
    } while ( y <= x);
	//print (y);
    return c; 
  }

  String getCl() {
    String res = randomItem(cl);
    while (res == _lastCl)
      res = randomItem(cl);
    _lastCl = res;
    return res;
  }
  
  String c() {
    int x = random.nextInt(cfTot);
    int h = 0;
    var lk = cf.keys.toList();
    lk.sort();
    int y = 0;
	  String c = "";
    do {
	  //print ("$vfTot $y");
      y+= cf[lk[h]];
	  c = lk[h];
      h++;
    } while ( y <= x);
	//print (y);
    return c; 
  }

  List<String> finalCl = [
    'lt','ld','ls',
    'rt','rd','rs'
  ];

  String getFinalCl() => finalCl[random.nextInt(finalCl.length)];

  List<String> dypt = [
    'ea','ea','ea','ea',
    'ei','ei','ai','ae',
    'ae','ai','ao','au',
    'ea','ei','eo','eu',
    'ia','ie','io','iu',
    'oa','oe','oi','ou',
    'ua','ue','ui','uo'
  ];

  String getDipt() => dypt[random.nextInt(dypt.length)];
  
  List<String> cl = [
	'br','bb','bbl','bbr',
	'cr','cl','ccr',
	'dr','dd','dl',
	'fr','fl','ff','ffr','ffl',
	'gr','gg','ggr','ggl',
	'kr','ks',
	'lg','lf','lt','ll','lf','lp',
	'mb','mf','mm',
	'nd','nf','nt','ntr','ndr','ntl','ndl',
	'pp','pr','pl',
	'rt','rd','rs','rr','rg','rf','rp','rv',
	'ss','sr','sl',
	'tt','tr',
	'vr'];

  String cv() => c()+ v();
  String vc() => v()+c();
  String vcv() => v()+c()+v();
  String cvc() => c()+v()+c();
  
  /* 1. cv
   * 2. vc
   * 3. cvc
   * 4. vcv
   * 5. cl
   * 6. c
   * 7. v
   */
  List<List<int>> patterns = [
    [1,3],
    [1,3],
    [1,3],
    [2,4,6],
    [2,4,6],
    [2,4,-6],
    [1,1,6],
    [1,1,6],
    [1,1,-6],
    [1,1,-6,-7],
    [1,-3,-7],
    [1,-3,2],
    [1,-3,2],
    [1,-3,-2,-7],
    [1,5,2],
    [1,5,2],
    [1,5,-2,-7],
  ];

  static final NameBuilder _singleton = new NameBuilder._internal();
  
  factory NameBuilder() {
    return _singleton;
  }

  bool roll (int odd, int res) => random.nextInt(odd) == res;
  String rollString (int odd, int res, String resTrue, String resFalse) => roll(odd, res) ? resTrue : resFalse;
  
  String createWord() {
    String res = "";
    var pIdx = random.nextInt(patterns.length);
    //print (pIdx);
    List<int> pattern = patterns[pIdx];
    pattern.forEach( (i) {
      switch (i) {
        case -1: res += c()+ rollString (4, 2, getDipt() , v()) ; break;
        case -2: res +=  rollString (4, 2, getDipt() , v()) +c(); break;
        case -3: res += c() + rollString (4, 2, getDipt() , v())  + c(); break;
        case -4: res += rollString (4, 2, getDipt() , v())  + c() + rollString (6, 2, getDipt() , v()) ; break;
        case -6: res += rollString(3,1,c(), getFinalCl()); break;
        case -7: res += rollString (3, 2, getDipt() , v()) ; break;
        case 1: res += cv(); break;
        case 2: res += vc(); break;
        case 3: res += cvc(); break;
        case 4: res += vcv(); break;
        case 5: res += getCl(); break;
        case 6: res += c(); break;
        case 7: res += v(); break;
        default: break;        
      }
    });
		return res;
  }

  String createName1() => capitalize(createWord());
  
  String person() => capitalize(createWord())+ " "+ capitalize(createWord());

  String realPlace() => DATA.listOfPlaces[random.nextInt(DATA.listOfPlaces.length)];

  String codePlace1() => "${c()+c()}-${c()+c()} ${1000+random.nextInt(9000)}".toUpperCase();


  String createName() {
    String res = realPlace();
    int roll = 1 + random.nextInt(20);
    if (roll < 13)
      res = createName1();
    else if (roll >= 13 && roll < 19)
      res = realPlace();
    else 
      res = codePlace1();
    return res;
  }

  NameBuilder._internal() {
    cf.keys.forEach( (k) => cfTot += cf[k]);
    vf.keys.forEach( (k) => vfTot += vf[k]);
  }
}

void main() {
  NameBuilder nb = new NameBuilder();
  for (var h = 0; h < 100; h++)
 		 print (nb.person());
  print ("${nb.vfTot} ${nb.cfTot}");
}
