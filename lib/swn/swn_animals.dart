import 'dart:convert';
import 'dart:math';
import 'swn_random.dart' as RND;
import 'swn_nomen.dart' as NOMEN;



const ANIMAL_DATA = '''
{
  "AnimalStyle": [
    {
      "Body": "Humanoid", 
      "Limb": "Wings", 
      "Weapon": "Teeth or mandibles", 
      "Skin": "Hard shell", 
      "Size": "Cat-sized"
    }, 
    {
      "Body": "Quadruped", 
      "Limb": "Many joints", 
      "Weapon": "Claws", 
      "Skin": "Exoskeleton", 
      "Size": "Wolf-sized"
    }, 
    {
      "Body": "Many-legged", 
      "Limb": "Tentacles", 
      "Weapon": "Poison", 
      "Skin": "Odd texture", 
      "Size": "Calf-sized"
    }, 
    {
      "Body": "Bulbous", 
      "Limb": "Opposable thumbs", 
      "Weapon": "Harmful discharge", 
      "Skin": "Molts regularly", 
      "Size": "Bull-sized"
    }, 
    {
      "Body": "Amorphous", 
      "Limb": "Retractable", 
      "Weapon": "Pincers", 
      "Skin": "Harmful to touch", 
      "Size": "Hippo-sized"
    }, 
    {
      "Body": "Roll twice", 
      "Limb": "Varying sizes", 
      "Weapon": "Horns", 
      "Skin": "Wet or slimy", 
      "Size": "Elephant-sized"
    }
  ], 
  "Appearance": [
    {
      "Look": "Amphibian", 
      "Description": "froggish or newtlike"
    }, 
    {
      "Look": "Bird", 
      "Description": "winged and feathered"
    }, 
    {
      "Look": "Fish", 
      "Description": "scaled and torpedo-bodied"
    }, 
    {
      "Look": "Insect", 
      "Description": "beetle-like or fly-winged"
    }, 
    {
      "Look": "Mammal", 
      "Description": "hairy and fanged"
    }, 
    {
      "Look": "Reptile", 
      "Description": "lizardlike and long-bodied"
    }, 
    {
      "Look": "Spider", 
      "Description": "many-legged and fat"
    }, 
    {
      "Look": "Exotic", 
      "Description": "made of wholly alien elements"
    }
  ], 
  "Predator": [
    "Hunts in kin-group packs", 
    "Favors ambush attacks", 
    "Cripples prey and waits for death", 
    "Pack supports alpha-beast attack", 
    "Lures or drives prey into danger", 
    "Hunts as a lone, powerful hunter", 
    "Only is predator at certain times", 
    "Mindlessly attacks humans"
  ], 
  "Scavenger": [
    "Never attacks unwounded prey", 
    "Uses other beasts as harriers", 
    "Always flees if significantly hurt", 
    "Poisons prey waits for it to die", 
    "Disguises itself as its prey", 
    "Remarkably stealthy", 
    "Summons predators to weak prey", 
    "Steals prey from weaker predator"
  ],  
  "Prey": [
    "Moves in vigilant herds", 
    "Exists in small family groups", 
    "They all team up on a single foe", 
    "They go berserk when near death", 
    "They're violent in certain seasons", 
    "They're vicious if threatened", 
    "Symbiotic creature protects them", 
    "Breeds at tremendous rates"
  ], 
  "Poison Table": [
    {
      "Onset": "Instant", 
      "Duration": "1d6 rounds", 
      "Poison": "Death"
    }, 
    {
      "Onset": "1 round", 
      "Duration": "1 minute", 
      "Poison": "Paralysis"
    }, 
    {
      "Onset": "1d6 rounds", 
      "Duration": "10 minutes", 
      "Poison": "1d4 dmg per onset interval"
    }, 
    {
      "Onset": "1 minute", 
      "Duration": "1 hour", 
      "Poison": "Convulsions"
    }, 
    {
      "Onset": "1d6 minutes", 
      "Duration": "1d6 hours", 
      "Poison": "Blindness"
    }, 
    {
      "Onset": "1 hour", 
      "Duration": "1d6 days", 
      "Poison": "Hallucinations"
    }
  ], 
  "Harmful Discharge": [
    "Acidic spew doing its damage on a hit", 
    "Toxic spittle or cloud", 
    "Super-heated or super-chilled spew", 
    "Sonic drill or other disabling noise", 
    "Natural laser or plasma discharge", 
    "Nauseating stench or disabling chemical", 
    "Equipment-melting corrosive", 
    "Explosive pellets or chemical catalysts"
  ]
}
''';

class SWNAppearance {
  String look;
  String description;
  SWNAppearance(this.look, this.description);
  @override
  String toString() {
      // TODO: implement toString
      return "$look -- $description";
    }
  String toHtml() {
      // TODO: implement toString
      return "<p class='look-western'>$look -- $description</p>";
    }
  
  Map<String, dynamic> toMap() => {
    'description' : description,
    'look' : look,
  };

  SWNAppearance.fromMap(dynamic d) {
    description = d["description"];
    look = d["look"];
  }
    
}

class SWNAnimalPoison {
  String onset;
  String duration;
  String poison;
  SWNAnimalPoison(this.onset, this.duration, this.poison);
  @override
  String toString() => "Poison: $poison; Onset: $onset; Duration: $duration";
  String toHtml() => "<p class='poison-western'>Poison: $poison; Onset: $onset; Duration: $duration</p>";

  Map<String, dynamic> toMap() => {
    'onset' : onset,
    'duration' : duration,
    'poison' : poison,
  };

  SWNAnimalPoison.fromMap(dynamic d) {
    onset = d["onset"];
    duration = d["duration"];
    poison = d["poison"];
  }

}

class SWNAnimalStyle {
  String body;
  String limb;
  String weapon;
  String skin;
  String size;
  SWNAnimalStyle(this.body, this.limb, this.weapon, this.skin, this.size);
  @override 
  String toString() => "Body: $body; Limb: $limb; Weapon: $weapon; Skin: $skin; Size: $size";
  String toHtml() => "<p class='animal-style-western'>Body: $body; Limb: $limb; Weapon: $weapon; Skin: $skin; Size: $size</p>";


  Map<String, dynamic> toMap() => {
    'body' : body,
    'limb' : limb,
    'weapon' : weapon,
    'skin' : skin,
    'size' : size,
  };

  SWNAnimalStyle.fromMap(dynamic d) {
    body = d["body"];
    limb = d["limb"];
    weapon = d["weapon"];
    skin = d["skin"];
    size = d["size"];
  }

}

class SWNAnimal {
  SWNAppearance appearance;
  SWNAnimalStyle animalStyle;
  SWNAnimalPoison animalPoison;
  String harmfulDischarge;
  String preyBehavior;
  String scavengerBehavior;
  String predatorBehavior;
  String name = new NOMEN.NameBuilder().createWord();

  Map<String, dynamic> toMap() {
    var m =  {
    'appearance' : appearance.toMap(),
    'animalStyle' : animalStyle.toMap(),
    'preyBehavior' : preyBehavior,
    'predatorBehavior' : predatorBehavior,
    'scavengerBehavior' : scavengerBehavior
    };
    if (animalPoison != null)
    m['animalPoison'] = animalPoison.toMap();
    if (harmfulDischarge != null) 
    m['harmfulDischarge'] = harmfulDischarge;
    m['name'] = name;
    return m;
  }

  SWNAnimal.fromMap(dynamic d) {
    var oPoison = d["animalPoison"];
    var oAppearance = d["appearance"];
    var oAnimalStyle = d["animalStyle"];
    predatorBehavior = d["predatorBehavior"];
    preyBehavior = d["preyBehavior"];
    scavengerBehavior = d["scavengerBehavior"];
    harmfulDischarge = d["harmfulDischarge"];
    if (oPoison != null) animalPoison = new SWNAnimalPoison.fromMap(oPoison);
    appearance = new SWNAppearance.fromMap(oAppearance);
    animalStyle = new SWNAnimalStyle.fromMap(oAnimalStyle);
    name = d['name'];
  }


  SWNAnimal({this.appearance, this.animalStyle, this.animalPoison, this.harmfulDischarge,this.predatorBehavior, this.preyBehavior, this.scavengerBehavior, this.name});
  @override
  String toString() {
      // TODO: implement toString
      StringBuffer buf = new StringBuffer();
      buf.writeln("Name: $name\nStyle: $animalStyle\nLook: $appearance\nAs a prey: $preyBehavior\nAs");
      if (animalPoison != null) buf.writeln("Poison: $animalPoison");
      if (harmfulDischarge != null) buf.writeln("Harmful discharge: $harmfulDischarge");
      return buf.toString();
    }

  String toHtml() {
    StringBuffer buf = new StringBuffer();
    buf.writeln ("<h3 class='animal-title'>$name</h3>\n");
    buf.writeln("<h4 class='animal-subtitle-western'>Style:</h4> ${animalStyle.toHtml()}");
    buf.writeln("<h4 class='animal-subtitle-western'>Look:</h4> ${appearance.toHtml()}");
    buf.writeln("<h4 class='animal-subtitle-western'>Behavior as a predator:</h4><p class='behavior-western'>$predatorBehavior</p>");
    buf.writeln("<h4 class='animal-subtitle-western'>Behavior as a prey:</h4><p class='behavior-western'>$preyBehavior</p>");
    buf.writeln("<h4 class='animal-subtitle-western'>Behavior as a scavenger:</h4><p class='behavior-western'>$scavengerBehavior</p>");
    if (animalPoison != null)
      buf.writeln("<h4 class='animal-subtitle-western'>Poison</h4>${animalPoison.toHtml()}");
    if (harmfulDischarge != null)
      buf.writeln("<h4 class='animal-subtitle-western'>Discharge attack:</h4><p class='attack-western'>$harmfulDischarge</p>");
    return buf.toString();
  }
}


class SWNAnimalBuilder {
  static final SWNAnimalBuilder _singleton = new SWNAnimalBuilder._internal();

  List<SWNAppearance> _appearances = [];
  List<SWNAnimalStyle> _styles= [];
  List<SWNAnimalPoison> _poisons= [];
  List<String> _predatorBehavior= [];
  List<String> _preyBehavior= [];
  List<String> _scavengerBehavior= [];
  List<String> _discharges= [];
  List<String> _validBodies=[] ;

  factory SWNAnimalBuilder() {
    return _singleton;
  }

  SWNAnimalBuilder._internal() {
    var jsonObject = jsonDecode(ANIMAL_DATA);
    List<dynamic> dStyles = jsonObject["AnimalStyle"];
    List<dynamic> dAppearances = jsonObject["Appearance"];
    List<dynamic> dDischarges = jsonObject["Harmful Discharge"];
    List<dynamic> dPoison = jsonObject["Poison Table"];
    List<dynamic> dPrey = jsonObject["Prey"];
    List<dynamic> dPredator = jsonObject["Predator"];
    List<dynamic> dScavenger = jsonObject["Scavenger"];

    _scavengerBehavior = dScavenger.cast<String>();
    _preyBehavior = dPrey.cast<String>();
    _predatorBehavior = dPredator.cast<String>();
    _discharges=dDischarges.cast<String>();
    dStyles.forEach( (m) {
      var a = new SWNAnimalStyle(m["Body"], m["Limb"], m["Weapon"], m["Skin"], m["Size"]);
      _styles.add(a);
    });
    dPoison.forEach( (p) {
      var z = new SWNAnimalPoison(p["Onset"], p["Duration"], p["Poison"]);
      _poisons.add(z);
    });
    dAppearances.forEach( (a) {
      var app = new SWNAppearance(a["Look"], a["Description"]);
      _appearances.add(app);
    });
    _validBodies =  _styles.map( (b) => b.body).where( (m) => m != 'Roll twice').toList().cast<String>();

  }

  SWNAnimalStyle _getRandomStyle() => _styles[RND.rnd.nextInt(_styles.length)];
  SWNAnimalStyle _createRandomStyle() {
    var r = new SWNAnimalStyle(
    _getRandomStyle().body, 
    _getRandomStyle().limb, 
    _getRandomStyle().weapon, 
    _getRandomStyle().skin, 
    _getRandomStyle().size
    );
    if (!_validBodies.contains(r.body)) {
      _validBodies.shuffle();
      var t2 = _validBodies.take(2).join(" with ") + " features";
      r.body = t2;
    }
    return r;
  }

  String _getDischarges() => _discharges[RND.rnd.nextInt(_discharges.length)];
  String _getPredatorBehavior() => _predatorBehavior[RND.rnd.nextInt(_predatorBehavior.length)];
  String _getPreyBehavior() => _preyBehavior[RND.rnd.nextInt(_preyBehavior.length)];
  String _getScavengerBehavior() => _scavengerBehavior[RND.rnd.nextInt(_scavengerBehavior.length)];
  SWNAppearance _getRandomAppearance() => _appearances[RND.rnd.nextInt(_appearances.length)];
  SWNAnimalPoison _getRandomPoison() => _poisons[RND.rnd.nextInt(_poisons.length)];
  SWNAnimalPoison _createRandomPoison() => new SWNAnimalPoison(
    _getRandomPoison().onset, 
    _getRandomPoison().duration, 
    _getRandomPoison().poison
    );
  
  SWNAnimal buildAnimal() {
    SWNAnimal a = new SWNAnimal();
    a.name = new NOMEN.NameBuilder().createWord();
    a.animalStyle = _createRandomStyle();
    a.appearance = _getRandomAppearance();
    a.predatorBehavior = _getPredatorBehavior();
    a.preyBehavior = _getPreyBehavior();
    a.scavengerBehavior = _getScavengerBehavior();
    if (a.animalStyle.weapon == "Poison") a.animalPoison = _createRandomPoison();
    if (a.animalStyle.weapon == "Harmful discharge") a.harmfulDischarge = _getDischarges();
    return a;
  }
}
